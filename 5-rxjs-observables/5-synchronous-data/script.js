/**
 * This example doesn’t have anything asynchronous about it
 * We simply use RxJS observables here to easily retrieve a value out of an object.
 */

const observable = Rx.Observable.of({name: 'Arnaldo'});

observable
  .pluck('name')
  .subscribe(
      (data) => console.log(data)
    );
