/**
 * Being able to react to an infinite amount of asynchronous (or also synchronous!)
 * Here are the two biggest arguments for using observables over promises
 * You’re working with streams of data instead of single values
 *      The data source (for example a button getting clicked) may emit multiple values in different moments
 * You got an amazing toolset of operators at your disposal to manipulate, transform and work with your async data
 */

 /**
  * Ejemplo: stream of data
  */
const button = document.querySelector('button')
/**
 * el observable observa los eventos click del boton 
 * clicks on that button are our data source now
 */

const observable = Rx.Observable.fromEvent(button, 'click') 

/**
 *  The function we pass to subscribe() gets executed whenever a new value is emitted
 */
observable.subscribe(event => {
    console.log(event.target)
})