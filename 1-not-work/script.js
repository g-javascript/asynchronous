const getUser =  () => {
    setTimeout(() => {
        return {name: 'Arnaldo'}
    }, 2000);
}

const user = getUser(); // obtiene el resultado inmediatamente y el valor inmediato es void
console.log(user.name); // javascript ejecuta de manera no bloqueante, no espera a que el código asincrono ejecute y retorne un valor