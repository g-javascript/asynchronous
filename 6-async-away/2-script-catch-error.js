const checkAuth = () => {
    console.log('checkAuth inicia')
    return new Promise(resolve => {
      setTimeout(() => {
        console.log('checkAuth resuelve')
        resolve(true);
    }, 2000);
      console.log('checkAuth siguiente a promesa')
    });
  };
  
  const getUser = (auth) => {
    console.log('getUser inicia')
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        if (auth) {
            console.log('getUser resuelve')
            resolve({name: 'Arnaldo'});
        } else {
          reject('Not logged in!');
        }
      }, 2000);
      console.log('getUser  siguiente a promesa')
    });
  };
  
  /**
   * ASYNC to turn it into an async function. 
   */
  async function fetchUser() {
    try {
        console.log('fetchUser inicia')
        // AWAIT simply waits for a promise to resolve. 
        // It basically does the same then() does
        const auth = await checkAuth(); // <- async operation
        console.log('fetchUser siguiente a await auth')
        const user = await getUser(auth); // <- async operation
        console.log('fetchUser siguiente a await user')
        return user;            
    } catch (error) {
        return {name: 'Default'}
    }
  }
  
  fetchUser().then((user) => console.log(user.name));
  