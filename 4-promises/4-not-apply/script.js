const button = document.querySelector('button')

const handleClick = () => {
  console.log('1')
  //  You could handle one event (e.g. click on a button) 
  // but thereafter your promise is resolved and can’t react to further clicks.
  return new Promise(resolve => { 
    //  adding an event listener to a button in our DOM.
    // This happens inside the promise but since a promise can only resolve once, 
    // we’re only reacting to the first click
    button.addEventListener('click', () => { 
      resolve(event)
    })
  })
}

// Para ver el retorno de la promesa. Le asocie la llamada al boton en el index para notar que el objeto de la promesa solo se crea una vez
handleClick().then(event => {
  console.log('2', event.target)
})
