/**
 *  Promises: introducidas por ES6
 *  Util cuando se utilizan operaciones asincronas dependientes
 * 
 */

const getUser = () => {
    console.log('1')
    return new Promise((resolve, reject) => { // Promise puede recibir dos funciones como parametros: resolve y reject denominadas comunmente
        setTimeout(() => {
            reject('se produjo un error') // completa la promesa y retorna el dato a la funcion dentro de then
        }, 2000);
        console.log('2')
    })
 }

 getUser().then(user => {
    console.log('3', user.name)
 })
 .catch(error => {
    console.log('3', error) // handle error here
})