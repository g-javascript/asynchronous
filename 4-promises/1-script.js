/**
 *  Promises: introducidas por ES6
 *  Ventaja: Util cuando se utilizan operaciones asincronas dependientes
 *  Desventaja: Solo puedes manejar una operacion asincrona con cada promesa
 *  Usos:
 *      Soliciudes HTTP
 *  No aplicable:
 *      Oepraciones asincronas las cuales no finalizan despues de un valor
 * 
 */

 const getUser = () => {
    console.log('1')
    return new Promise(resolve => { // Promise puede recibir dos funciones como parametros: resolve y reject denominadas comunmente
         setTimeout(() => {
             resolve({name: 'Arnaldo'}) // completa la promesa y retorna el dato a la funcion dentro de then
         }, 2000);
         console.log('2')
    })
 }

 getUser().then(user => {
    console.log('3', user.name)
 })