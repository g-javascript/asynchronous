const checkAuth = () => {
    console.log('1')
    return new Promise(resolve => {
        setTimeout(() => {
            resolve({isAuth: true})
        }, 2000);
        console.log('2')
    })
}

const getUser = () => {
    console.log('4')
    return new Promise(resolve => { // Promise puede recibir dos funciones como parametros: resolve y reject denominadas comunmente
        setTimeout(() => {
            resolve({name: 'Arnaldo'}) // completa la promesa y retorna el dato a la funcion dentro de then
        }, 2000);
        console.log('5')
    })
}

checkAuth().then(authStatus => {
    console.log('3')
    return getUser(authStatus);
})
.then(user => {
    console.log('6', user.name)
})