const getUser = cb => {
    setInterval(() => {
        cb({name: 'Arnaldo'}) // Se le pasa un argumento a la función callback
    }, 2000);
}

getUser(user => { // se pasa como parametro una función (callback) la cual se ejecutara una vez que se complete el timeout
    console.log(user.name)
})
console.log('This prints before "Arnaldo" gets printed!')