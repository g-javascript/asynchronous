/* 
    Callbacks: Pensados para simples operaciones asincronas
    Ejemplo: Un caso donde se dificulta el análisis de un callback
*/
const checkAuth = cb => {
    console.log('1')
    setTimeout(() => {
        cb({isAuth: true})
    }, 2000);
    console.log('2')
}

const getUser = (auth, cb) => {
    console.log('4')
    if(!auth.isAuth) {
        cb(null)
        return
    }
    setTimeout(() => {
        cb({name: 'Arnaldo'})
    }, 2000);
    console.log('5')
}

checkAuth(authInfo => {
    console.log('3')
    getUser(authInfo, user => {
        console.log('7', user.name)
    })
    console.log('6')
})